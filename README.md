# README #

This is a repository a for a code created to visualize results from the software JML in figure 2 of the publication "Conflicting phylogenomic signals reveal a pattern of reticulate evolution in a recent high-Andean diversification (Asteraceae: Astereae: Diplostephium)"


### What is this repository for? ###

* the R code presented here will generate a visual matrix of significant results sorted based on
your phylogeny
* I provide two files necessary to have create the visual matrix: a tree and the JML output
* Version 6

### How do I get set up? ###

* Run heat_mapv6.R
* If you have your own data just replace the input files