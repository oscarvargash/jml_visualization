dir()
#install.packages("Matrix")
library(Matrix)
#install.packages("devtools")
library(devtools)
#install_github("liamrevell/phytools")
library(phytools)


tree <- read.nexus("nrDNA_90_m1_final_allcom.con.tre")
rtree <- root(tree, "Soliva_sessilis")
lrtree <- ladderize(rtree, right = T)

plot(lrtree, cex=0.5)

tip.order <- lrtree$tip.label[lrtree$edge[lrtree$edge[,2] <= Ntip(lrtree)]]


comparisons <- read.table("Results_v3.txt", header =T)

matrix <- xtabs(Probability ~ seq1 + seq2, data=comparisons)
#matchr <- match(rownames(matrix), tip.order)
#matchc <- match(colnames(matrix), tip.order)

#heatmap(matrix, Rowv = NA, Colv = NA, reorderfun = reorder (matchr, matchc))

matrix0 <- data.frame(c(0), nrow=91, ncol=91,)

colnames(matrix0) <- c("Diplostephium_callilepis","Diplostephium_goodspeedii","Diplostephium_lechleri","Diplostephium_haenkei","Diplostephium_sp_nov_OXA","Diplostephium_sp_nov_JUN4","Diplostephium_pulchrum_PAS","Diplostephium_pulchrum_OXA","Diplostephium_sp_nov_JUN2","Diplostephium_sp_nov_CAJ","Diplostephium_gnidioides","Diplostephium_sp_nov_JUN","Diplostephium_gynoxyoides","Diplostephium_hippophae","Diplostephium_sp_nov_JUN3","Diplostephium_barclayanum","Diplostephium_foliosissimum","Diplostephium_sagasteguii","Diplostephium_oxapampanum","Diplostephium_espinosae","Diplostephium_hartwegii","Diplostephium_ericoides","Diplostephium_crypteriophyllum","Diplostephium_juniperinum","Diplostephium_spinulosum","Diplostephium_empetrifolium","Diplostephium_glandulosum","Diplostephium_oblanceolatum","Diplostephium_jelskii","Diplostephium_cajamarquillense","Diplostephium_serratifolium","Diplostephium_azureum","Diplostephium_meyenii","Floscaldasia_hypsophila","Parastrephia_quadrangularis","Baccharis_tricuneata","Heterothalamus_alienus","Baccharis_genistelloides","Diplostephium_sp_nov_CAJ2","Diplostephium_cinereum","Laennecia_sophiifolia","Westoniella_kohkemperi","Aztecaster_matudae","Exostigma_notobellidiastrum","Diplostephium_obtusum","Diplostephium_venezuelense","Diplostephium_cinerascens","Diplostephium_violaceum","Diplostephium_cayambense","Diplostephium_lacunosum","Diplostephium_revolutum","Diplostephium_phylicoides","Diplostephium_rosmarinifolium","Diplostephium_floribundum","Diplostephium_heterophyllum","Diplostephium_alveolatum","Diplostephium_costaricense","Diplostephium_rhomboidale_COL","Diplostephium_apiculatum","Diplostephium_rhomboidale_ECU","Diplostephium_schultzii_CAL","Diplostephium_schultzii_CUN","Diplostephium_juajibioyi","Diplostephium_rhododendroides","Diplostephium_frontinense","Diplostephium_jenesanum","Diplostephium_tenuifolium","Diplostephium_camargoanum","Diplostephium_tachirense","Diplostephium_ochraceum","Diplostephium_sp_nov_ANT","Diplostephium_antioquense","Diplostephium_oblongifolium","Diplostephium_mutiscuanum","Diplostephium_eriophorum","Diplostephium_rupestre","Diplostephium_jaramilloi","Diplostephium_huertasii","Diplostephium_glutinosum","Diplostephium_coriaceum","Diplostephium_romeroi","Diplostephium_inesianum","Diplostephium_colombianum","Lagenophora_cuchumatanica","Blakiella_bartsiifolia","Hinterhubera_ericoides","Laestadia_muscicola","Archibaccharis_asperifolia","Oritrophium_peruvianum","Llerasia_caucana","Soliva_sessilis")

rownames(matrix0) <- rev(c("Diplostephium_callilepis","Diplostephium_goodspeedii","Diplostephium_lechleri","Diplostephium_haenkei","Diplostephium_sp_nov_OXA","Diplostephium_sp_nov_JUN4","Diplostephium_pulchrum_PAS","Diplostephium_pulchrum_OXA","Diplostephium_sp_nov_JUN2","Diplostephium_sp_nov_CAJ","Diplostephium_gnidioides","Diplostephium_sp_nov_JUN","Diplostephium_gynoxyoides","Diplostephium_hippophae","Diplostephium_sp_nov_JUN3","Diplostephium_barclayanum","Diplostephium_foliosissimum","Diplostephium_sagasteguii","Diplostephium_oxapampanum","Diplostephium_espinosae","Diplostephium_hartwegii","Diplostephium_ericoides","Diplostephium_crypteriophyllum","Diplostephium_juniperinum","Diplostephium_spinulosum","Diplostephium_empetrifolium","Diplostephium_glandulosum","Diplostephium_oblanceolatum","Diplostephium_jelskii","Diplostephium_cajamarquillense","Diplostephium_serratifolium","Diplostephium_azureum","Diplostephium_meyenii","Floscaldasia_hypsophila","Parastrephia_quadrangularis","Baccharis_tricuneata","Heterothalamus_alienus","Baccharis_genistelloides","Diplostephium_sp_nov_CAJ2","Diplostephium_cinereum","Laennecia_sophiifolia","Westoniella_kohkemperi","Aztecaster_matudae","Exostigma_notobellidiastrum","Diplostephium_obtusum","Diplostephium_venezuelense","Diplostephium_cinerascens","Diplostephium_violaceum","Diplostephium_cayambense","Diplostephium_lacunosum","Diplostephium_revolutum","Diplostephium_phylicoides","Diplostephium_rosmarinifolium","Diplostephium_floribundum","Diplostephium_heterophyllum","Diplostephium_alveolatum","Diplostephium_costaricense","Diplostephium_rhomboidale_COL","Diplostephium_apiculatum","Diplostephium_rhomboidale_ECU","Diplostephium_schultzii_CAL","Diplostephium_schultzii_CUN","Diplostephium_juajibioyi","Diplostephium_rhododendroides","Diplostephium_frontinense","Diplostephium_jenesanum","Diplostephium_tenuifolium","Diplostephium_camargoanum","Diplostephium_tachirense","Diplostephium_ochraceum","Diplostephium_sp_nov_ANT","Diplostephium_antioquense","Diplostephium_oblongifolium","Diplostephium_mutiscuanum","Diplostephium_eriophorum","Diplostephium_rupestre","Diplostephium_jaramilloi","Diplostephium_huertasii","Diplostephium_glutinosum","Diplostephium_coriaceum","Diplostephium_romeroi","Diplostephium_inesianum","Diplostephium_colombianum","Lagenophora_cuchumatanica","Blakiella_bartsiifolia","Hinterhubera_ericoides","Laestadia_muscicola","Archibaccharis_asperifolia","Oritrophium_peruvianum","Llerasia_caucana","Soliva_sessilis"))

head(matrix0)
rownames(matrix0)

matrix1 <- merge(matrix0, matrix, by = intersect(colnames, rownames))




matrix3 <- matrix0
mcol <- match(colnames(matrix),colnames(matrix0))






for (name in rownames(matrix0)){
	
}


sparseMatrix(i=comparisons$seq1, j=comparisons$seq2, x=comparisons$Probability)



matrix2 <- matrix[order(rownames(matrix)),order(colnames(matrix))]
head(matrix2)

matrix[,colnames]

matrix2 <- matrix[order(matrix[,1],matrix[,2],decreasing=T),]


heatmap(matrix, Rowv = NA, Colv = NA)


phylo.heatmap(lrtree,matrix)